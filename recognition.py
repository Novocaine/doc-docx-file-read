import re

def generator(content):
    p = re.compile(r'^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$')
    results = p.findall(content)
    for result in results:
        print(result)

def check_id_card(idCard):
    # p= re.compile(r'[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]')
    # ret = p.match(idCard)
    ret = re.finditer(r'[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]',idCard)
    # ret = re.findall(r'[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]',idCard)
    if ret:
        return ret
    return

def verify_mobile(phone_number):
    ret = re.finditer(r'(1[3456789]\d)(\d{4})(\d{4})',phone_number)
    if ret:
        return ret
