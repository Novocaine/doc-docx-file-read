import sys
import pickle
import re
import  codecs
import string
import shutil
from win32com.client import Dispatch
import docx
 
 
def doSaveAas(path):
    word = Dispatch('Word.Application')
    doc = word.Documents.Open(path)        # 目标路径下的文件
    _rename_path = path[:-3]+'docx'
    doc.SaveAs(_rename_path, 12, False, "", True, "", False, False, False, False)  # 转化后路径下的文件    
    doc.Close()
    word.Quit()
    return _rename_path