import docx
import recognition

# document = docx.Document('demo.docx')
class SearchIdAndPhoneNumber:
    #初始化文档
    document = docx.Document()
    def __init__(self,str):
        self.document = docx.Document(str)
    #查找段落
    def search_paragraphs(self):
        for para in self.document.paragraphs:
            if recognition.check_id_card(para.text):
                results = recognition.check_id_card(para.text)
                for result in results:
                    print(result.group())
                    # print( '<Match: %r, groups=%r>' % (result.group(), result.groups()))
                    # print(result.string)
            if recognition.verify_mobile(para.text):
                results = recognition.verify_mobile(para.text)
                for result in results:
                    print(result.group())
        
    #查找表
    def search_tables(self):
        for table in self.document.tables:
            for row in table.rows:
                for cell in row.cells:
                    if recognition.verify_mobile(cell.text):
                        results = recognition.verify_mobile(cell.text)
                        for result in results:
                            print(result.group())
                    if recognition.check_id_card(cell.text):
                        results = recognition.check_id_card(cell.text)
                        for result in results:
                            print(result.group())
    def search(self):
        self.search_paragraphs()
        self.search_tables()

# for i in range(len(document.paragraphs)):
#     print("第"+str(i)+"段的内容是："+document.paragraphs[i].text)
